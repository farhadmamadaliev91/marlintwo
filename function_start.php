<?php
session_start();
function get_user_by_email ($email) {

    $pdo = new PDO("mysql:host=localhost:3306; dbname=myproject", "root", "");
    $sql = "SELECT * FROM users WHERE  email=:email";
    $statement = $pdo->prepare($sql);
    $statement -> execute([
        'email'=>$email
    ]);
    $user = $statement->fetch(PDO::FETCH_ASSOC);
    return $user;

}

function add_user($email, $password) {
    $pdo = new PDO("mysql:host=localhost:3306; dbname=myproject", "root", "");
    $sql = "INSERT INTO users(email, password) values (:email, :password)";
    $statement = $pdo->prepare($sql);
    $result= $statement -> execute([
        'email'=>$email,
        'password'=>$password
//            password_hash($password, PASSWORD_DEFAULT)
    ]);

    return $pdo->lastInsertId();

}

function set_flash_message($name, $message) {

    $_SESSION[$name]=$message;
}

function display_flash_message($name) {
  if(isset($_SESSION[$name])) {
     echo "<div class=\"alert alert-{$name} text-dark\" role=\"alert\">{$_SESSION[$name]}</div>";
        unset($_SESSION[$name]);

  }
}

function redirect_to($path) {
    header("Location:{$path}");
    exit;
}

function check_authorization ($email, $password)
{
    $pdo = new PDO("mysql:host=localhost:3306; dbname=myproject", "root", "");
    $sql = "SELECT * FROM users WHERE  email=:email and password=:password";
    $statement = $pdo->prepare($sql);
    $statement->execute([
        'email' => $email,
        'password' => $password
    ]);
    $user = $statement->fetch(PDO::FETCH_ASSOC);


    if ($user) {
        header("Location:/users.php");
    } else {
        set_flash_message('danger', 'Данный логин или пароль отсуттвуют');
        redirect_to("page_login.php");
    }

}



function check_admin_button ($email, $password)
{
    $pdo = new PDO("mysql:host=localhost:3306; dbname=myproject", "root", "");
    $sql = "SELECT * FROM users WHERE  email=:email and password=:password";
    $statement = $pdo->prepare($sql);
    $statement->execute([
        'email' => $email,
        'password' => $password
    ]);
     $user = $statement->fetch(PDO::FETCH_ASSOC);
     return $user;

}

function edit_information ($id, $name, $job, $phone, $address) {
    $pdo = new PDO("mysql:host=localhost:3306; dbname=myproject", "root", "");
    $sql = "UPDATE users SET name = :name, job = :job, phone = :phone, address = :address WHERE id = :id";
    $statement = $pdo->prepare($sql);
    $result= $statement -> execute([
        'id'=>$id,
        'name'=>$name,
        'job'=>$job,
        'phone'=>$phone,
        'address'=>$address
//
    ]);
    $statement->fetchAll(PDO::FETCH_ASSOC);

}

function edit_credentials ($id, $email, $password) {
    $pdo = new PDO("mysql:host=localhost:3306; dbname=myproject", "root", "");
    $sql = "UPDATE users SET email = :email, password = :password WHERE id = :id";
    $statement = $pdo->prepare($sql);
    $result= $statement -> execute([
        'id'=>$id,
        'email'=>$email,
        'password'=>$password
    ]);
    $statement->fetchAll(PDO::FETCH_ASSOC);

}

function set_status ($id, $status) {
    $pdo = new PDO("mysql:host=localhost:3306; dbname=myproject", "root", "");
    $sql = "UPDATE users SET status = :status WHERE id = :id";
    $statement = $pdo->prepare($sql);
    $result= $statement -> execute([
        'id'=>$id,
        'status'=>$status
    ]);
    $statement->fetchAll(PDO::FETCH_ASSOC);
}

function set_file ($id, $file) {
    $pdo = new PDO("mysql:host=localhost:3306; dbname=myproject", "root", "");
    $sql = "UPDATE users SET file = :file WHERE id = :id";
    $statement = $pdo->prepare($sql);
    $result= $statement -> execute([
        'id'=>$id,
        'file'=>$file['name']
    ]);
    $path = 'uploads/'.$_FILES['file']['name'];
    move_uploaded_file($file['tmp_name'], $path);
}

function upload_avatar($file, $id){

    $pdo = new PDO("mysql:host=localhost:3306; dbname=myproject", "root", "");
    $sql = "UPDATE users SET file = :file WHERE id = :id";
    $statement = $pdo->prepare($sql);
    $statement->execute([
        'id' => $id,
        'file'=>$file['name']
    ]);
    $path = 'uploads/'.$file['name'];
    move_uploaded_file($file['tmp_name'], $path);

}


function add_social_links ($id, $vk, $telegram, $instagram) {
    $pdo = new PDO("mysql:host=localhost:3306; dbname=myproject", "root", "");
    $sql = "UPDATE users SET vk = :vk, telegram = :telegram, instagram = :instagram  WHERE id = :id";
    $statement = $pdo->prepare($sql);
    $result= $statement -> execute([
        'id'=>$id,
        'vk'=>$vk,
        'telegram'=>$telegram,
        'instagram'=>$instagram
    ]);
    $statement->fetchAll(PDO::FETCH_ASSOC);
}




//function no_edit() {
//    if ($_SESSION['user']['role']=='admin')
//    { echo "<div class=\"dropdown-menu\">
//                                        <a class=\"dropdown-item\" href=\"edit.php\">
//                                            <i class=\"fa fa-edit\"></i>
//                                        Редактировать</a>
//                                        <a class=\"dropdown-item\" href=\"security.php\">
//                                            <i class=\"fa fa-lock\"></i>
//                                        Безопасность</a>
//                                        <a class=\"dropdown-item\" href=\"status.php\">
//                                            <i class=\"fa fa-sun\"></i>
//                                        Установить статус</a>
//                                        <a class=\"dropdown-item\" href=\"media.php\">
//                                            <i class=\"fa fa-camera\"></i>
//                                            Загрузить аватар
//                                        </a>
//                                        <a href=\"#\" class=\"dropdown-item\" onclick=\"return confirm('are you sure?');\">
//    <i class=\"fa fa-window-close\"></i>
//    Удалить
//                                        </a>
//                                    </div>";};
//
//}

//function no_edit_user() {
//    if ($_SESSION['user']['role']=='admin' or $_SESSION['user']['role']=='user')
//    { echo "<div class=\"dropdown-menu\">
/*                                        <a class=\"dropdown-item\" href=\"http://localhost:8081//edit.php?id=<?php echo $item['id']?>\">*/
//                                            <i class=\"fa fa-edit\"></i>
//                                        Редактировать</a>
//                                        <a class=\"dropdown-item\" href=\"security.html\">
//                                            <i class=\"fa fa-lock\"></i>
//                                        Безопасность</a>
//                                        <a class=\"dropdown-item\" href=\"status.html\">
//                                            <i class=\"fa fa-sun\"></i>
//                                        Установить статус</a>
//                                        <a class=\"dropdown-item\" href=\"media.html\">
//                                            <i class=\"fa fa-camera\"></i>
//                                            Загрузить аватар
//                                        </a>
//                                        <a href=\"#\" class=\"dropdown-item\" onclick=\"return confirm('are you sure?');\">
//    <i class=\"fa fa-window-close\"></i>
//    Удалить
//                                        </a>
//                                    </div>";};
//
//}

function get_user_by_id ($id) {

    $pdo = new PDO("mysql:host=localhost:3306; dbname=myproject", "root", "");
    $sql = "SELECT * FROM users WHERE  id=:id";
    $statement = $pdo->prepare($sql);
    $statement -> execute([
        'id'=>$id
    ]);
    $user = $statement->fetch(PDO::FETCH_ASSOC);
    return $user;
}




function check_authorization1 ($email)
{
    $pdo = new PDO("mysql:host=localhost:3306; dbname=myproject", "root", "");
    $sql = "SELECT * FROM users WHERE  email=:email";
    $statement = $pdo->prepare($sql);
    $statement->execute([
        'email' => $email,
    ]);
    $user = $statement->fetch(PDO::FETCH_ASSOC);
    return $user;

}

function has_image ($user) {
    if (!empty($user['file'])) {
        echo "uploads/{$user['file']}";
    } elseif (empty($user['file'])) {
        echo "img/demo/authors/josh.png";
    }
}

function delete ($id) {

    $pdo = new PDO("mysql:host=localhost:3306; dbname=myproject", "root", "");
    $sql = "DELETE FROM `users` WHERE id = :id";
    $statement = $pdo->prepare($sql);
    $statement->execute(
        [
            'id'=>$id
        ]
    );
}
