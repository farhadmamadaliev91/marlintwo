<?php

session_start();
require 'function_start.php';

$email = $_POST['email'];
$password= $_POST['password'];
$name =$_POST['name'];
$job =$_POST['job'];
$phone =$_POST['phone'];
$address =$_POST['address'];
$status =$_POST['status'];
$file = $_FILES['file'];
$vk = $_POST['vk'];
$telegram = $_POST['telegram'];
$instagram = $_POST['instagram'];

$id = add_user($email, $password);
$_SESSION['id']=$id;

edit_information($id, $name, $job, $phone, $address);
set_status ($id, $status);

//$path = 'uploads/'.$_FILES['file']['name'];
//$ext = pathinfo($path, PATHINFO_EXTENSION);

set_file($id, $file);

add_social_links($id, $vk,$telegram,$instagram);
redirect_to("users.php");