<?php
session_start();
require "function_start.php";

$id = $_GET['id'];
$user = $_SESSION['user'];


if ($id == $user['id']) {
    delete ($id);
    redirect_to("page_login.php");
} else {
    delete ($id);
    redirect_to("users.php");
    set_flash_message("success", 'Позьзователь удален');
}

